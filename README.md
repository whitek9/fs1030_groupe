# FS1030 Group E Project

## To get the project running: 

1. Follow the steps in the README in the "EMR-app/src/emr-backend" directory to get the API running and set up

2. Run `$ npm install` in the "EMR-app" directory to install dependencies

3. Initialize the React application by running `$ npm start` in the "EMR-app" directory

## Submissions

1. EER and IFD can be found in the "PDFs" folder

2. Source Code for front end is in EMR-App/src, and for the API in `EMR-App/src/emr-backend`

3. Database scheme export can be found in the "PDFs" folder with the EER and IFD

4. Query information below - all queries are in the files at the following path: `EMR-App/src/emr-backend/src/routes`

## SQL Query information

|Name |File Path |Route Description |Query Line No.|
--- | --- | --- | ---
|Kelly|EMR-app/src/emr-backend/src/routes/userRoutes.js|Query careproviderinfo table for password to allow login|25|
|Kelly|EMR-app/src/emr-backend/src/routes/patientRoutes.js|Query patientprofile table to search for a patient|17|
|Kelly|EMR-app/src/emr-backend/src/routes/patientRoutes.js|Query to delete a patient result|41|
|Kelly|EMR-app/src/emr-backend/src/routes/patientRoutes.js|Query to pull patient profile history and JOIN with careproviderinfo|63|
|Kelly|EMR-app/src/emr-backend/src/routes/careProviderRoutes.js|Query careproviderinfo table to search for a careprovider|17|
|Kelly|EMR-app/src/emr-backend/src/routes/careProviderRoutes.js|Query to delete a care provider|41|
|Kelly|EMR-app/src/emr-backend/src/routes/jaysRoutes.js|Query to pull notes from treatementrecord table and JOIN careprovider info|17|
|Sneha|EMR-app/src/emr-backend/src/routes/addPatient.js|Query to add the patient information to ‘patientProfile’ Table|32|
|Sneha|EMR-app/src/emr-backend/src/routes/addPatient.js|Query to addnew patient HCN to ‘patientreport’ Table|37|
|Sneha|EMR-app/src/emr-backend/src/routes/addProvider.js|Query to add the Care Provider information to ‘careproviderinfo’ Table|20|
|Sneha|EMR-app/src/emr-backend/src/routes/addPatient.js|Query to check whether the health card number exist in the ‘patientProfile’ Table|18|
|Rebecca|EMR-app/src/emr-backend/src/routes/patientRoutes.js|Query to join patient profile table and patient report table to display patient info on patient details page|89|
|Rebecca/Jay|EMR-app/src/emr-backend/src/routes/patientRoutes.js|Query to get all(including notes) of patient from patientprofile, patientreport, patienttreatmentrecord tables|74|
|Jay|EMR-app/src/emr-backend/src/routes/jaysRoutes.js|Query to post new notes into patienttreatmentrecord table|35|
|Jay|EMR-app/src/emr-backend/src/routes/jaysRoutes.js|Query to put updated notes into patienttreatmentrecord table|50|
|Jay|EMR-app/src/emr-backend/src/routes/jaysRoutes.js|Query to put updated changes to patient in patientchangerecord table|63|
|Jay|EMR-app/src/emr-backend/src/routes/jaysRoutes.js|Query to put updated changes to patient medical history to patientreport table|73|