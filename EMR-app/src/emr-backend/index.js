import express from 'express'
import cors from 'cors'
import userRouter from './src/routes/userRoutes.js'
import patientRouter from './src/routes/patientRoutes.js'
import careProviderRouter from './src/routes/careProviderRoutes.js'
import addPatient from "./src/routes/addPatient.js"
import addProvider from "./src/routes/addProvider.js"



import jaysRoutes from './src/routes/jaysRoutes.js'

const app = express()
const PORT = process.env.PORT || 4000

app.use(cors())

app.use(express.json())

app.use(userRouter)

app.use(patientRouter)

app.use(careProviderRouter)

app.use(addPatient)

app.use(addProvider)

app.use(jaysRoutes)

app.get("*", (req, res, next) => {
    
    let err = new Error("user typed non-existent URL")
    
    next(err)
})

app.listen(PORT, () => console.log(`API server ready on http://localhost:${PORT}`))
