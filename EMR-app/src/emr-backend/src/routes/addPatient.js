import express from 'express'
import * as validation from '../middleware/validationFunctions.js'
import connection from '../database/connection.js'
import { useReducer } from 'react'
const bcrypt = require("bcrypt");
const saltRounds = bcrypt.genSaltSync(10);

const addPatient = express.Router()

//sneha's route to add all patients for patientprofile

addPatient.post("/addpatient", (req, res) => {

    const isEmpty = arr => !Array.isArray(arr) || arr.length === 0;
    let health = req.body.health

    connection.query(
        "SELECT * FROM patientProfile WHERE HCN = ?",
        [health],
        function (error, results, fields) {

            if (!isEmpty(results)) {
                console.log("already there:");
                return res
                    .status(400)
                    .json({ message: 'HCN already added' })

            } else {


                //insert data to the patientprofile
                connection.query("INSERT INTO patientProfile(HCN, `firstName`, `lastName`, `email`, `dob`, `phoneNumber`) VALUES ( ?, ?, ?, ?, ?, ? )",
                    [req.body.health, req.body.firstName, req.body.lastName, req.body.email, req.body.dob, req.body.phoneNumber],
                    function (error, results, fields) {
                        if (error) throw error;
                        else {
                            connection.query("INSERT INTO patientreport(HCN) VALUES ( ?)",
                                [req.body.health],
                                function (error, results, fields) {
                                    if (error) throw error;
                                    return res.status(201).send(results);

                                });
                        }


                    });
            }
        })



});

export default addPatient;

