import express from 'express'
import { v4 as uuidv4 } from 'uuid'
import * as validation from '../middleware/validationFunctions.js'
import connection from '../database/connection'

const careProviderRouter = express.Router()
const bcrypt = require('bcrypt')
const saltRounds = 10

// Kelly's route to search for care provider result
careProviderRouter.get('/care_provider/:id', async (req,res) => {
    
    const isEmpty = arr => !Array.isArray(arr) || arr.length === 0;
    let id = req.params.id

    connection.query(
        "SELECT * FROM careproviderinfo WHERE careProviderID = ?",
        [id],
        function (error, results, fields) {

            if (isEmpty(results)) {
                return res
                .status(400)
                .json( [{ message: 'Employee Not Found' }] )

            } else {
                return res
                .status(201)
                .json(results)
            }
        })
    }
)

// Kelly's route to delete care provider result
careProviderRouter.delete('/care_provider/:id', async (req,res) => {
    
    let id = req.params.id

    connection.query(
        "DELETE FROM careproviderinfo WHERE careProviderID = ?",
        [id],
        function (error, results, fields) {
            if (results.affectedRows == 0) {
                return res
                .status(400)
                .json( { message: 'Employee Not Found' } )
            } else {
                return res
                .status(201)
                .json( { message: 'Employee Deleted' } )
            }
        })
    }
)

export default careProviderRouter;