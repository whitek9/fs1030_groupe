import express from 'express'
import * as validation from '../middleware/validationFunctions.js'
import connection from '../database/connection.js'

const patientRouter = express.Router()

// Kelly's route to search for patient result
patientRouter.get('/patient/:id', async (req,res) => {
    
    const isEmpty = arr => !Array.isArray(arr) || arr.length === 0;
    let id = req.params.id

    connection.query(
        "SELECT * FROM patientprofile WHERE HCN = ?",
        [id],
        function (error, results, fields) {

            if (isEmpty(results)) {
                return res
                .status(400)
                .json( [{ message: 'Patient Not Found' }] )

            } else {
                return res
                .status(201)
                .json(results)
            }
        }
    )
})

// Kelly's route to delete patient result
patientRouter.delete('/patient/:id', async (req,res) => {
    
    let id = req.params.id

    connection.query(
        "DELETE FROM patientprofile WHERE HCN = ?",
        [id],
        function (error, results, fields) {
            if (results.affectedRows == 0) {
                return res
                .status(400)
                .json( { message: 'Patient Not Found' } )
            } else {
                return res
                .status(201)
                .json( { message: 'Patient Deleted' } )
            }
        })
    }
)

//Kelly's route to pull patient profile history
patientRouter.get('/patient/profile_history/:id', (req,res) => {
    
    let id = req.params.id

    connection.query(
        "SELECT patientchangerecord.HCN, patientchangerecord.updatedFields, patientchangerecord.previousText, patientchangerecord.newText, patientchangerecord.date, patientchangerecord.careProviderID, careproviderinfo.firstName, careproviderinfo.lastName, careproviderinfo.specialization FROM patientchangerecord JOIN careproviderinfo ON patientchangerecord.careproviderid=careproviderinfo.careproviderid WHERE HCN = ? ORDER BY date DESC",
        [id],
        function (error, results, fields) {
            return res
            .status(201)
            .json(results)
        }
    )
})



//Rebecca's route to pull all patient information for patient details page 
//   + Jays - pulls notes
patientRouter.get('/patients/:id', (req, res) => {
    const sqlQuery=`select * from patientprofile inner join patientreport on patientprofile.HCN=patientreport.HCN inner join patienttreatmentrecord on patienttreatmentrecord.HCN=patientreport.HCN where patientprofile.HCN=${req.params.id} ORDER BY date DESC`;
    connection.query(sqlQuery, (err, rows) => {
        if(err) throw err
        res.json(rows[0])
    })
})

patientRouter.get('/patients/:id', (req, res) => {
    const sqlQuery=`select * from patienttreatmentrecord where patientprofile.HCN=${req.params.id} ORDER BY date DESC`;
    connection.query(sqlQuery, (err, rows) => {
        if(err) throw err
        res.json(rows[0])
    })
})




export default patientRouter;

