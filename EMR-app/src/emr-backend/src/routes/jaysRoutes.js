import express from 'express'
import * as validation from '../middleware/validationFunctions.js'
import connection from '../database/connection.js'

const jaysRoutes = express.Router()



//Jays route  3. View all the notes on a patients file in order to understand their treatment history ****

//Kelly's route to pull treatment history
jaysRoutes.get('/patient/treatment_history/:id', (req,res) => {

    let id = req.params.id

    connection.query(
        "SELECT patienttreatmentrecord.note, patienttreatmentrecord.date, patienttreatmentrecord.careproviderid, careproviderinfo.firstname, careproviderinfo.lastname, careproviderinfo.specialization FROM patienttreatmentrecord JOIN careproviderinfo ON patienttreatmentrecord.careproviderid = careproviderinfo.careproviderid WHERE HCN = ? ORDER BY date DESC",
        [id],
        function (error, results, fields) {
            return res
            .status(201)
            .json(results)
        }
    )
})


  //************ */


  //Jays route  1. Add notes to a patients file in order to keep their status up to date  *****


  jaysRoutes.post("/patient/:id/add-notes", (req, res) => {
    connection.query("INSERT INTO patienttreatmentrecord (HCN, careproviderID, note) VALUES (?, ?, ?)", [req.params.id, req.body.careproviderID, req.body.note ], function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    });
  });


  //********* */


  // 2. Update any section of my client's file in order to make sure their profile and history are up to date



  jaysRoutes.put("/update-notes", (req, res) => {
    connection.query(`INSERT INTO patienttreatmentrecord WHERE HCN=${req.params.HCN}`, function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    });
  });


// Update Routes



jaysRoutes.put("/patients/:id/update-changerecord", (req, res) => {
    connection.query(
      `INSERT INTO patientchangerecord SET HCN=${req.params.HCN}, careProviderID, updatedFields="", previousText="", newText=""`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });

  jaysRoutes.put("/patients/:id/update-patient", (req, res) => {
    connection.query(
      `UPDATE patientreport SET medicalhistory = ?, allergies = ?, demographic = ?, immunizationstatus = ?, radiologyimages = ? WHERE HCN=${req.params.id}`,
      [req.body.medicalHistory, req.body.allergies, req.body.demographic, req.body.immunizationStatus, req.body.radiologyImages],
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });

//   jaysRoutes.put("/patients/:id/update-patient", (req, res) => {
//     const { email } = req.body;
//     connection.query(
//       `UPDATE patientprofile SET email="${email}" WHERE id=${req.params.id}`,
//       function (error, results, fields) {
//         if (error) throw error;
//         return res.status(200).send(results);
//       }
//     );
//   });


// jaysRoutes.put("/patients/:id/update-patient", (req, res) => {
//     const { note } = req.body;
//     connection.query(
//       `UPDATE patienttreatmentrecord SET note="${note}" WHERE id=${req.params.id}`,
//       function (error, results, fields) {
//         if (error) throw error;
//         return res.status(200).send(results);
//       }
//     );
//   });



export default jaysRoutes