import express from 'express'
import * as validation from '../middleware/validationFunctions.js'
import connection from '../database/connection.js'
import { useReducer } from 'react'
const bcrypt = require("bcrypt");
const saltRounds = bcrypt.genSaltSync(10);

const addProvider = express.Router()

//sneha's route to add all care provider information to careproviderinfo
addProvider.post("/addprovider", (req, res) => {
    //insert data to the careproviderinfo

    const password = req.body.password;
    bcrypt.hash(password, saltRounds, (err, hash) => {

        if (err) {
            console.log(err)
        }
        connection.query("INSERT INTO careProviderInfo(firstName, lastName, email,specialization,isAdmin,password) VALUES ( ?, ?, ?, ?, ?, ? )", [req.body.firstName, req.body.lastName, req.body.email, req.body.specialization, req.body.isAdmin, hash], function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        });
    })

});

export default addProvider;

