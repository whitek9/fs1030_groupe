import React, { useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom'
import { Container, Row, Col, 
    Form, InputGroup, InputGroupAddon, InputGroupText, 
    Button, FormGroup, Label, Input} from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
//import '../../css/contact.css';

const LoginPage = () => {

    // base loginSubmit function leveraged from example-master file provided in the sample from the course material

    let history = useHistory()
    let location = useLocation()

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [auth, setAuth] = useState(true)
    const [showPassword, setShowPassword] = useState(false)

    const loginSubmit = async event => {
        
        event.preventDefault()

        const response = await fetch('http://localhost:4000/auth', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({email, password})
        })

        const payload = await response.json()

        if (response.status >= 400) {
            setAuth(false)
            document.getElementById('loginError').innerHTML = payload.message
        } else {
            sessionStorage.setItem('token', payload.token)

            let { from } = location.state || { from: { pathname: `/admin/${payload.id}` } };
            history.replace(from);
        }
        
    }

    return (
        <main>
            <Container className="themed-container centered">
                {!auth && 
                    <div>
                        <p><span className='red' id='loginError'></span></p>
                    </div>
                }
                <h3>Log in</h3>
                <Row>
                    <Col>
                        <Form onSubmit={loginSubmit}>
                            <FormGroup>
                                <Label for="Email">Email Address</Label>
                                <Input 
                                    type="email" 
                                    name="email" 
                                    placeholder="yourname@domain.com" 
                                    required
                                    value={email} 
                                    onChange={e => setEmail(e.target.value)} 
                                    autoFocus
                                />
                            </FormGroup>
                            <FormGroup>
                                <Label for="Password">Password</Label>
                                <InputGroup>
                                    <Input 
                                        type={showPassword ? 'text' : 'password'} 
                                        name="name" 
                                        required 
                                        value={password} 
                                        onChange={e => setPassword(e.target.value)} 
                                    />
                                    <InputGroupAddon addonType="append">
                                        <InputGroupText onClick = {() => {
                                            setShowPassword(!showPassword)
                                        }}>
                                            <FontAwesomeIcon icon='eye'/>
                                        </InputGroupText>
                                    </InputGroupAddon>
                                </InputGroup>
                            </FormGroup>
                            <Button type="submit" color="primary">Submit</Button> 
                        </Form>
                    </Col>
                    <Col>
                    </Col>
                </Row>
            </Container>
        </main>
    );
}
 
export default LoginPage;