import React, { useState, useEffect } from 'react';
import {useParams, useHistory} from "react-router-dom";
import { Button, Table} from 'reactstrap';

const CareProviderDetails = () => {

    let { id, careprovider} = useParams()
    const [careProvider, setCareProvider] = useState({})
    const history = useHistory();

    useEffect(() => {
        fetch(`http://localhost:4000/care_provider/${id}`)
        .then(response => response.json())
        .then((data) => {
            setCareProvider(data[0])
        })
    }, [id])

    // Go Back function
    const goBack = () => {
        history.push(`/admin/${careprovider}`)
    }

    return (
        <div>
            <h1>{careProvider.firstName} {careProvider.lastName}</h1>
            <Button outline color="primary" onClick={goBack}>Go Back</Button>
            <Table>
                <thead>
                    <tr>
                        <th>ID Number</th>
                        <th>Email</th>
                        <th>Specialization</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{careProvider.careProviderID}</td>
                        <td>{careProvider.email}</td>
                        <td>{careProvider.specialization}</td>
                    </tr>
                </tbody>
            </Table>
        </div>
    )
}


export default CareProviderDetails;