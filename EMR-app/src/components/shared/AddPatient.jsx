import React, {useState} from 'react';
import {useHistory} from 'react-router-dom'
import { Form, FormGroup, Col,Button, Input, Label, Container } from 'reactstrap'
const AddPatient = () => {
    
    let history = useHistory()
  
    const [health, sethealth] = useState("")
    const [firstName, setfirstName] = useState("")
    const [lastName, setlastName] = useState("")
    const [email, setEmail] = useState("")
    const [dob, setdob] = useState("")
    
    const [phoneNumber, setphoneNumber] = useState("")
   
    const handleSubmit = async event => {
        
        event.preventDefault()

        const response = await fetch('http://localhost:4000/addpatient', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
      
            body: JSON.stringify({health,firstName,lastName,email,dob,phoneNumber}),
          })
         

           if (response.status >= 400) {
            alert(`Patient Already added`)
        } else {
            alert(`Added the Patient information Successfully`)
            sethealth("");
            setfirstName("");
            setlastName("");
            setdob("");
            setphoneNumber("");
            setEmail("");
        }

        history.goBack()
    }
    return (
      <Container>
       
          <Form className="my-5" onSubmit={handleSubmit}>
          <FormGroup row>
                  <Label for="health" sm={2}>Health Card Number</Label>
                  <Col sm={10}>
                  <Input type="text" name="health" id="health" placeholder="Enter the patient health card number" required value={health} onChange={e => sethealth(e.target.value)} />
                  </Col>
              </FormGroup>
              <FormGroup row>
                    <Label for="nameEntry" sm={2}>First Name</Label>
                    <Col sm={10}>
                    <Input type="text" name="firstName" id="nameEntry" placeholder="Enter patient first name" required value={firstName}  onChange={e => setfirstName(e.target.value)} />
                    </Col>
                </FormGroup>
              <FormGroup row>
                  <Label for="nameEntry2" sm={2}>Last Name</Label>
                  <Col sm={10}>
                  <Input type="text" name="lastName" id="nameEntry2" placeholder="Enter patient Last name" required value={lastName} onChange={e => setlastName(e.target.value)} />
                  </Col>
              </FormGroup>
          <FormGroup row>
                  <Label for="emailEntry" sm={2}>Email</Label>
                  <Col sm={10}>
                  <Input type="email" name="email" id="emailEntry" placeholder="Enter email to contact"  required value={email}    onChange={e => setEmail(e.target.value)} />
                  </Col>
              </FormGroup>
           
              <FormGroup row>
                  <Label for="dob" sm={2}>Date of Birth</Label>
                  <Col sm={10}>
                  <Input type="date" name="dob" id="dob" placeholder="Enter Date of Birth" value={dob}    onChange={e => setdob(e.target.value)} />
                  </Col>
              </FormGroup>
             

              <FormGroup row>
                  <Label for="phoneNumber" sm={2}>Phone Number</Label>
                  <Col sm={10}>
                  <Input type="text" name="phoneNumber" id="phoneNumber" placeholder="Enter Phone Number" required value={phoneNumber} onChange={e => setphoneNumber(e.target.value)} />
                  </Col>
              </FormGroup>

           

              <FormGroup check row>
                  <Col sm={{ size: 10, offset: 2 }}>
                 
                  <Button  color="primary">Submit</Button>
                  </Col>
              </FormGroup>
          </Form>
      </Container>
    )
  }



export default AddPatient;