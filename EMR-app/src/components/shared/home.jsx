import React from 'react';
import { Container, Button, Col, Row } from 'reactstrap';
import { NavLink } from 'react-router-dom';
 
const home = () => {
    return (
        <main className="homePage">
            <Container className="themed-container centered">
                <Row>
                    <Col xs="6"></Col>
                    <Col xs="6">
                        <Row className="text-right">
                            <h3>
                                FS Hospital Electronic Records Management (FSHEMR)
                            </h3>
                        </Row>
                        <NavLink to="/login">
                            <Button className="float-right" color="primary">
                                LOGIN
                            </Button>
                        </NavLink>
                    </Col>
                </Row>
            </Container>
        </main>
    );
}
 
export default home;