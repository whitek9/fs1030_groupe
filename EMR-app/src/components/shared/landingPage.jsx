import React, { useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';

import { Container, Col, Row, Form,  
    Button, FormGroup, Label, Input, 
    Table} from 'reactstrap';


const AdminPage = () => {

    let location = useLocation()
    let pathname = location.pathname
    let providerID = pathname.substring(7)
    let history = useHistory();

    const token = sessionStorage.getItem('token')
    const [userInfo, setUserInfo] = useState([])
    const [admin, setAdmin] = useState(false)
    const [careProviderSearchID, setCareProviderSearchID] = useState("")
    const [careProviderSearchResults, setCareProviderSearchResults] = useState([])
    const [careProviderResult, setCareProviderResult] = useState(true)
    const [patientSearchID, setPatientSearchID] = useState("")
    const [patientSearchResults, setPatientSearchResults] = useState([])
    const [patientResult, setPatientResult] = useState(true)

    // UseEffect function to load authenticated users information
    useEffect(() => {
        const getUserData = async () => {
            const response = await fetch(`http://localhost:4000/care_provider/${providerID}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }          
            })
            const payload = await response.json()
            setUserInfo(payload)

            if (payload[0].isAdmin === 1) {
                setAdmin(true)
            }
        }
        getUserData()    
    }, [providerID, token])

    // Search Care Provider function by employee ID
    const searchCareProvider = async event => {
        
        event.preventDefault()
        const getCareProvider = async () => {
            const response = await fetch(`http://localhost:4000/care_provider/${careProviderSearchID}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }          
            })
            const payload = await response.json()

            if (response.status >= 400) {
                setCareProviderResult(false)
            } else {
                setCareProviderResult(true)
            }
            setCareProviderSearchResults(payload)
        }
        getCareProvider()    
    }

    // Search Patients function by health card number (10 digits)
    const searchPatient = async event => {
        
        event.preventDefault()
        const getPatient = async () => {
            const response = await fetch(`http://localhost:4000/patient/${patientSearchID}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }          
            })
            const payload = await response.json()
            
            if (response.status >= 400) {
                setPatientResult(false)
            } else {
                setPatientResult(true)
            }
            setPatientSearchResults(payload)
        }
        getPatient()    
    }

    // Delete care provider function
    const deleteCareProvider = async event => {

        let id = careProviderSearchResults[0].careProviderID
        
        event.preventDefault()
        const deleteRow = async () => {
            const response = await fetch(`http://localhost:4000/care_provider/${id}`, {
                method: 'DELETE',
                headers: {
                }          
            })
            const payload = await response.json()
            alert(payload.message)
            setCareProviderSearchResults([])
        }
        deleteRow()    
    }

    // Delete patient function
    const deletePatient = async event => {

        let id = patientSearchResults[0].HCN
        
        event.preventDefault()
        const deleteRow = async () => {
            const response = await fetch(`http://localhost:4000/patient/${id}`, {
                method: 'DELETE',
                headers: {
                }          
            })
            const payload = await response.json()
            alert(payload.message)
            setPatientSearchResults([])
        }
        deleteRow()    
    }

    // To add link to provider page with matching careProviderID
    const viewCareProvider = () => {
        history.push(`careproviders/${careProviderSearchID}/${providerID}`)
    }

    // To add link to patient details page with matching HCN
    const viewPatient = () => {
        history.push(`patients/${patientSearchID}/${providerID}`)
    }

    // To add link to add a care provider
    const addCareProvider = () => {
        history.push(`/admin/addprovider`)
    }

    // To add link to add a patient
    const addPatient = () => {
        history.push(`/admin/addpatient`)
    }

    // Logout function
    const logout = event => {
        event.preventDefault()
        sessionStorage.removeItem('token')
        history.push("/login")
    }

    // Format DOB function
    const formatDOB = (date) => {

        if (date) {
            const cleanDate = date.substring(0,10)
            return cleanDate
        } else {
            return date
        }
    }

    

    return (   
        <main class="containerColumn" id="page-wrap">
            <Container className="lessCentered">
                <Row>
                    <h2>Administrator Portal</h2> 
                </Row>
                <Row>
                    {userInfo.map((user) => 
                    <h4>Welcome {user.firstName} {user.lastName}, {user.specialization}</h4>)}
                    <Col xs={{offset: 11}}>
                        <Button outline color="primary" onClick={logout}>LOGOUT</Button>
                    </Col>
                </Row>
                {!admin &&
                    <div>
                        <Row>
                            <Col>
                                <Form onSubmit={searchPatient}>
                                    <FormGroup>
                                        <Label for="Search Patients">Search Patients by Health Card Number (10 digits)</Label>
                                        <Input 
                                            type="text" 
                                            name="name" 
                                            required 
                                            value={patientSearchID} 
                                            onChange={e => setPatientSearchID(e.target.value)} 
                                        />
                                    </FormGroup>
                                    <Button color="secondary" type="submit" >Search</Button>{' '} 
                                </Form> 
                                <Table borderless>
                                    <thead>
                                        <tr>
                                            <th>HCN</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>DOB</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {!patientResult && patientSearchResults.map((searchResult) => 
                                        <tr>
                                            <td colSpan='5'>
                                            {searchResult.message}
                                            </td>
                                        </tr>
                                    )}
                                    {patientResult === true && patientSearchResults.map((searchResult) => 
                                        <tr>
                                            <td>{searchResult.HCN}</td>
                                            <td>{searchResult.firstName}</td>
                                            <td>{searchResult.lastName}</td>
                                            <td>{formatDOB(searchResult.dob)}</td>
                                            <td><Button size="sm" color="info" onClick={viewPatient}>VIEW</Button></td>
                                            <td></td>
                                        </tr>
                                    )}
                                    </tbody>
                                </Table>
                            </Col>
                            <Col>
                            </Col>
                        </Row>  
                    </div>
                }       
                {admin && 
                    <div>
                        <Row>
                            <Col>
                                <Form onSubmit={searchPatient}>
                                    <FormGroup>
                                        <Label for="Search Patients">Search Patients by Health Card Number (10 digits)</Label>
                                        <Input 
                                            type="text" 
                                            name="name" 
                                            required 
                                            value={patientSearchID} 
                                            onChange={e => setPatientSearchID(e.target.value)} 
                                        />
                                    </FormGroup>
                                    <Button color="secondary" type="submit" >Search</Button>{' '}
                                    <Button color="primary" onClick={addPatient}>Add New Patient</Button>
                                </Form>
                                <Table borderless>
                                    <thead>
                                        <tr>
                                            <th>HCN</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>DOB</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {!patientResult && patientSearchResults.map((searchResult) => 
                                        <tr>
                                            <td colSpan='6'>
                                            {searchResult.message}
                                            </td>
                                        </tr>
                                    )}
                                    {patientResult &&  patientSearchResults.map((searchResult) => 
                                        <tr>
                                            <td>{searchResult.HCN || searchResult.message}</td>
                                            <td>{searchResult.firstName}</td>
                                            <td>{searchResult.lastName}</td>
                                            <td>{formatDOB(searchResult.dob)}</td>
                                            <td><Button size="sm" color="info" onClick={viewPatient}>VIEW</Button></td>
                                            <td><Button size="sm" outline color="danger" onClick={deletePatient}>DELETE</Button></td>
                                        </tr>
                                    )}
                                    </tbody>
                                </Table>
                            </Col>
                            <Col>
                                <Form onSubmit={searchCareProvider}>
                                    <FormGroup>
                                        <Label for="Search Care Provider">Search Care Provider by ID</Label>
                                            <Input 
                                                type="text" 
                                                name="name" 
                                                required 
                                                value={careProviderSearchID} 
                                                onChange={e => setCareProviderSearchID(e.target.value)} 
                                            /> 
                                    </FormGroup> 
                                    <Button color="secondary" type="submit" >Search</Button>{' '}
                                    <Button color="primary" onClick={addCareProvider}>Add New Care Provider</Button>
                                </Form>
                                <Table borderless>
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {!careProviderResult && careProviderSearchResults.map((searchResult) => 
                                        <tr>
                                            <td colSpan='6'>
                                            {searchResult.message}
                                            </td>
                                        </tr>
                                    )}
                                    {careProviderResult && careProviderSearchResults.map((searchResult) => 
                                        <tr>
                                            <td>{searchResult.careProviderID}</td>
                                            <td>{searchResult.firstName}</td>
                                            <td>{searchResult.lastName}</td>
                                            <td>{searchResult.email}</td>
                                            <td><Button size="sm" color="info" onClick={viewCareProvider}>VIEW</Button></td>
                                            <td><Button size="sm" outline color="danger" onClick={deleteCareProvider}>DELETE</Button></td>
                                        </tr>
                                    )}
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </div>
                }
            </Container>
        </main>
     );
 }
  
 export default AdminPage;