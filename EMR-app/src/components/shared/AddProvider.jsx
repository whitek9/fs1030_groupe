import React, { useState } from 'react'
import { Form, FormGroup, Col, Button, Input, Label, Container } from 'reactstrap'

import { useHistory } from 'react-router-dom'

const AddProvider = () => {
    //const [newPatient, setNewpatient] = useState({health: "", firstName: "" ,lastName:"",email:"",dob:"",phoneNumber:""});

    let history = useHistory()
    //let location = useLocation()


    const [firstName, setfirstName] = useState("")
    const [lastName, setlastName] = useState("")
    const [isAdmin, setisAdmin] = useState("")
    const [email, setEmail] = useState("")
    const [password, setpassword] = useState("")

    const [specialization, setspecialization] = useState("")

    const handleSubmit = async event => {

        event.preventDefault()

        const response = await fetch('http://localhost:4000/addprovider', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify({ firstName, lastName, email, specialization, isAdmin, password }),
        })
        if (response.status >= 400) {
            alert(`Ops !Error`)
        } else {
            alert(`Added the Care provider information Successfully`)
            setfirstName("");
            setlastName("");
            setEmail("");
            setisAdmin("");
            setpassword("");
        }

        history.goBack()
    }

    return (
        <Container>

            <Form className="my-5" onSubmit={handleSubmit}>
                <FormGroup row>
                    <Label for="nameEntry" sm={2}>First Name</Label>
                    <Col sm={10}>
                        <Input type="text" name="firstName" id="nameEntry" placeholder="Enter your first name" required value={firstName} onChange={e => setfirstName(e.target.value)} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="nameEntry2" sm={2}>Last Name</Label>
                    <Col sm={10}>
                        <Input type="text" name="lastName" id="nameEntry2" placeholder="Enter your Last name" required value={lastName} onChange={e => setlastName(e.target.value)} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="emailEntry" sm={2}>Email</Label>
                    <Col sm={10}>
                        <Input type="email" name="email" id="emailEntry" placeholder="Enter email to contact" required value={email} onChange={e => setEmail(e.target.value)} />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="specialization" sm={2}>Specialization</Label>
                    <Col sm={10}>
                        <Input type="text" name="specialization" id="specialization" placeholder="Enter Specialization" value={specialization} onChange={e => setspecialization(e.target.value)} />
                    </Col>
                </FormGroup>


                <FormGroup row>
                    <Label for="isAdmin" sm={2}>Is Admin</Label>
                    <Col sm={10}>
                        <Input type="select" name="isAdmin" id="isAdmin"  value={isAdmin} onChange={e => setisAdmin(e.target.value)}>
                            <option>1</option>
                            <option>0</option>
                            

                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="password" sm={2}>Password</Label>
                    <Col sm={10}>
                        <Input type="password" name="password" id="password" required value={password} onChange={e => setpassword(e.target.value)} />
                    </Col>
                </FormGroup>

                <FormGroup check row>
                    <Col sm={{ size: 10, offset: 2 }}>

                        <Button color="primary">Submit</Button>
                    </Col>
                </FormGroup>
            </Form>
        </Container>
    )
}

export default AddProvider