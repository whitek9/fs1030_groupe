import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const EditPatient = (props) => {
  let { id } = useParams()
  const [patients, setPatients] = useState([]);

  const {
    className } = props;

  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  useEffect(() => {

    async function fetchData() {
      const res = await fetch(`http://localhost:4000/patients/${id}`);
      res.json().then((res) => setPatients(res));

    }
    fetchData();
  }, [id]);

  const handleChange = (event) => {
    console.log([event.target.name], event.target.value)
    setPatients((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    fetch(`http://localhost:4000/patients/${id}/update-patient`, {
      method: "put",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(patients),
    }).then((response) => response.json());
    toggle();
    window.location.reload()
  };

  return (
    <div>
        <div>
            <Button color="danger" onClick={toggle}>Edit Patient Details</Button>
            <Modal isOpen={modal} toggle={toggle} className={className}>
                <ModalHeader toggle={toggle}>Edit Patient Details</ModalHeader>
                <ModalBody>
                    {Object.entries(patients).map(([key, value]) => (
                    <div key={patients.id}>
                        <p></p>
                        <label htmlFor={key}>{key}</label>
                        <input
                            type="text"
                            name={key}
                            value={patients[key]}
                            onChange={handleChange}
                        />
                    </div>
                ))}        
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={handleSubmit}>Submit</Button>{' '}
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    </div>
  );
};

export default EditPatient;
