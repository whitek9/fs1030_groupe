import React, { useState, useEffect } from "react";
import {useHistory, useParams} from 'react-router-dom'

import { Button, Table} from 'reactstrap';


const ViewNotes = () => {
    let { id } = useParams();


  const [treatmentData, setTreatmentData] = useState([]);
  const history = useHistory();


  useEffect(() => {

    async function fetchData() {
      const res = await fetch(`http://localhost:4000/patient/treatment_history/${id}`);
      res.json().then((res) => setTreatmentData(res));
    
    }
    fetchData();
  }, [id]);

  // Format date function
  const formatDate = (date) => {

    if (date) {
        const cleanDate = date.substring(0,10)
        return cleanDate
    } else {
        return date
    }
}

  // Go Back function
  const goBack = () => {
    history.goBack()
}

  return (
    <main>
        <h3>Treatment History</h3>
        <Button outline color="primary" onClick={goBack}>Go Back</Button>
        <Table striped>
            <thead>
                <tr>
                    <th>Note</th>
                    <th>Care Provider</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
            {treatmentData.map((data) => 
                <tr>
                    <td>{data.note}</td>
                    <td>{data.lastname}, {data.firstname}, {data.specialization}</td>
                    <td>{formatDate(data.date)}</td>
                </tr>
            )}
            </tbody>
        </Table>
    </main>
  );
};

export default ViewNotes;
