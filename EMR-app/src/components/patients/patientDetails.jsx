import React, { useState, useEffect } from 'react';
import {useParams, useHistory} from "react-router-dom";
import { Button, Table} from 'reactstrap';
import EditPatient from "./editPatient"
import AddPatientNotes from './addPatientNotes';

const PatientDetails = () => {
    let { id, careprovider, patientEdit } = useParams()
    const [patient, setPatient] = useState({})

    const IDinfo = {
        patientID:id, 
        careproviderID: careprovider
    }

    const IDinfoEdit = {
        patientID:id, 
        patientEditID: patientEdit
    }

    const history = useHistory();


    useEffect(() => {
        fetch(`http://localhost:4000/patients/${id}`)
        .then(response => response.json())
        .then((data) => {
            setPatient(data)
        })
    }, [id])

    const viewChangeHistory = () => {
        history.push(`/admin/history/${id}`)
    }

      const viewTreatmentHistory = () => {
        history.push(`/admin/treatment_history/${id}`)
    }

    // Go Back function
    const goBack = () => {
        history.push(`/admin/${careprovider}`)
    }

    return (
        <div>
            <h1>File for: {patient.firstName} {patient.lastName}</h1>
            <Button outline color="primary" onClick={goBack}>Go Back</Button>
            <Table>
                <thead>
                    <tr>
                        <th>HCN</th>
                        <th>DOB</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Demographic</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{patient.HCN}</td>
                        <td>{patient.dob?.slice(0, 10)}</td>
                        <td>{patient.email}</td>
                        <td>{patient.phoneNumber}</td>
                        <td>{patient.demographic}</td>
                        <td></td>
                    </tr>
                </tbody>
            </Table>

            <Table>
                <thead>
                    <tr>
                        <th>Allergies</th>
                        <th>Medical History</th>
                        <th>Immunization Status</th>
                        <th>Radiology Images</th>
                        <th>Care Provider ID</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{patient.allergies}</td>
                        <td>{patient.medicalHistory}</td>
                        <td>{patient.immunizationStatus}</td>
                        <td>{patient.radiologyImages}</td>
                        <td>{careprovider}</td>
                    </tr>
                </tbody>
            </Table>
            <div className="button-div">
                <Button color="primary" onClick={viewTreatmentHistory}>View Treatment Notes</Button>{" "}
                <Button color="primary" onClick={viewChangeHistory}>View Change History</Button>
                <AddPatientNotes  IDinfo={IDinfo} />
                <EditPatient IDinfoEdit={IDinfoEdit} />
            </div>
        </div>
    )
  }

export default PatientDetails;