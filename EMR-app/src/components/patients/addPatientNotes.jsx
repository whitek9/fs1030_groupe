import React, {useState} from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';


const AddPatientNotes = (props) => {
    const [newNote] = useState({note: ""});
    const [patient, setPatient] = useState({})

    const info = props.IDinfo
  
    const [modal, setModal] = useState(false);
  
    const toggle = () => setModal(!modal);
    

     const handleChange = (event) => {
       console.log(info)
       setPatient((prevState) => ({
         ...prevState,
         careproviderID: info.careproviderID,
         [event.target.name]: event.target.value,
       }));
     };

     const handleSubmit = (event) => {
        fetch(`http://localhost:4000/patient/${info.patientID}/add-notes`, {
        method: "post",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },

        body: JSON.stringify(patient),
        }).then((response) => response.json());
        toggle();
        window.location.reload()
    };

    return (
      <div>

      <div>
        <Button color="danger" onClick={toggle}>Add to Patient Notes</Button>
        <Modal isOpen={modal} toggle={toggle} >
          <ModalHeader toggle={toggle}>Add to Notes</ModalHeader>
          <ModalBody>
          {Object.keys(newNote).map(patient => (
          <div key={patient.id}>
            <p>{patient.email}</p>
            <input
                type="text"
                name="note"
                value={patient.note}
                onChange={handleChange}
              />
          </div>
        ))}        
        </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={handleSubmit}>Submit</Button>{' '}
            <Button color="secondary" onClick={toggle}>Cancel</Button>
          </ModalFooter>
      </Modal>
      </div>
        <form onSubmit={(e) => handleSubmit(e, patient.id)}>
        </form>
      </div>
    );
}

export default AddPatientNotes;