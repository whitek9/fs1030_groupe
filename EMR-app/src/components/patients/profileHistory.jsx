import React, { useEffect, useState } from 'react';
import {useHistory, useParams} from "react-router-dom";

import { Button, Table} from 'reactstrap';


const ProfileHistory = () => {
    let { id } = useParams();

    const token = sessionStorage.getItem('token')
    const [treatmentData, setTreatmentData] = useState([])
    let history = useHistory();
    
    useEffect(() => {
        const getData = async () => {
            const response = await fetch(`http://localhost:4000/patient/profile_history/${id}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            const data = await response.json()
            setTreatmentData(data)
        }
        getData()
    }, [token, id])

    // Format date function
    const formatDate = (date) => {

        if (date) {
            const cleanDate = date.substring(0,10)
            return cleanDate
        } else {
            return date
        }
    }

    // Go Back function
    const goBack = () => {
        history.goBack()
    }
                                
    return (
        <main>
            <h3>Profile History</h3>
            <Button outline color="primary" onClick={goBack}>Go Back</Button>
            <Table striped>
                <thead>
                    <tr>
                        <th>Field(s) Changed</th>
                        <th>Previous Information</th>
                        <th>New Information</th>
                        <th>Care Provider</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                {treatmentData.map((data) => 
                    <tr>
                        <td>{data.updatedFields}</td>
                        <td>{data.previousText}</td>
                        <td>{data.newText}</td>
                        <td>{data.lastName}, {data.firstName}, {data.specialization}</td>
                        <td>{formatDate(data.date)}</td>
                    </tr>
                )}
                </tbody>
            </Table>
        </main>
    )
}

export default ProfileHistory;
                                