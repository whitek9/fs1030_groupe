import React, { useState, useEffect } from 'react';
import {Route, Switch, useParams, useHistory} from "react-router-dom";
import { Button, Table} from 'reactstrap';
import ViewNotes from "./viewNotes"
import EditPatient from "./EditPatient"
import AddPatientNotes from './addPatientNotes';

const PatientDetails = () => {
    let { id, careprovider, patientEdit } = useParams()
    const [patient, setPatient] = useState({})

    const IDinfo = {
        patientID:id, 
        careproviderID: careprovider
    }

    const IDinfoEdit = {
        patientID:id, 
        patientEditID: patientEdit
    }

    const history = useHistory();


    useEffect(() => {
        fetch(`http://localhost:4000/patients/${id}`)
        .then(response => response.json())
        .then((data) => {
            setPatient(data)
        })
    }, [])

    const refreshPage = ()=>{
        window.location.reload();
     }

    return (
        <div>
            <h1>{patient.firstName} {patient.lastName}</h1>
            <Table>
                <thead>
                    <tr>
                        <th>HCN</th>
                        <th>DOB</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Demographic</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{patient.HCN}</td>
                        <td>{patient.dob?.slice(0, 10)}</td>
                        <td>{patient.email}</td>
                        <td>{patient.phoneNumber}</td>
                        <td>{patient.demographic}</td>
                        <td></td>
                    </tr>
                </tbody>
            </Table>
            <div className="button-div">
                <AddPatientNotes  IDinfo={IDinfo} />
                <EditPatient IDinfoEdit={IDinfoEdit} />
            </div>

            <div>HCN:Care Provider ID: {careprovider}</div>
            <div>Allergies: {patient.allergies}</div>
            <div>Immunization Status: {patient.immunizationStatus}</div>
            <div>Medical History: {patient.medicalHistory}</div>
            <div>Radiology Images: {patient.radiologyImages}</div>
            <div>Patient Notes: {patient.note}</div>
        </div>
    )
  }

export default PatientDetails;