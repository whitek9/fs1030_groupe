import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faEye } from '@fortawesome/free-solid-svg-icons'

import Home from './components/shared/home'
import LoginPage from './components/login/login'
import SecureRoute from './components/login/secureRoute'
import AdminPage from './components/shared/landingPage'
import PatientDetails from './components/patients/patientDetails'
import ProfileHistory from './components/patients/profileHistory'
import CareProviderDetails from './components/careproviders/careProviderDetails'
import addPatient from './components/shared/addPatient'
import addProvider from './components/shared/addProvider'
import EditPatient from './components/patients/editPatient'
import ViewNotes from './components/patients/viewNotes'

import './App.css';


library.add(faEye)

function App() {
  return (
    <BrowserRouter>
        <Switch>
          <Route path='/' component={Home} exact/>
          <Route path='/login' component={LoginPage}/>
          <Route path='/admin/history/:id'  component={ProfileHistory}/>
          <Route path='/admin/treatment_history/:id'  component={ViewNotes}/>
          <Route path='/admin/patients/:id/:careprovider' component={PatientDetails}/>
          <Route path='/admin/careproviders/:id/:careprovider' component={CareProviderDetails}/>
          <Route path='/admin/patients/:id/edit' component={EditPatient}/>
          <Route path='/admin/addpatient' component={addPatient} exact/>
          <Route path='/admin/addprovider' component={addProvider}/>
          <SecureRoute path='/admin'>
            <AdminPage />
          </SecureRoute>      
          <Route component={Error}/>
        </Switch>
    </BrowserRouter>
  );
}

export default App;
